FROM dtr.docker.si2m.tec/tools-store/si2m_nodejs

WORKDIR /opt/app

COPY package*.json ./

RUN npm install --only=production
COPY . .

EXPOSE 3000
CMD ["npm", "start"]