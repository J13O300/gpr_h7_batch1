# language: fr
Fonctionnalité: La page d'accueil

# GPR-1
Plan du scénario: Je veux tester mon root endpoint
  Etant donné que j'appelle <uri> en <methode>
  Alors le retour est égal à <output>
	Et le code de retour est égal à <code>

	Exemples:
		| methode | uri |            output           | code |
		|     GET |   / | "Bienvenue dans GPR_H7_batch!" |  200 |


