const { setWorldConstructor } = require('cucumber');

// Loads custom types
require('./parameter_types');

// Starts server
require('../../lib');

class CustomWorld {
}

setWorldConstructor(CustomWorld);
