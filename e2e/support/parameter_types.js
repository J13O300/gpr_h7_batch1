const { defineParameterType } = require('cucumber');

defineParameterType({
  name: 'httpMethod',
  regexp: /GET|POST|PUT|PATCH|DELETE/,
});

defineParameterType({
  name: 'uri',
  regexp: /[a-z0-9/#?+=&-]+/,
});
